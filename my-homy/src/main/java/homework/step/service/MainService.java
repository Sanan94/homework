package homework.step.service;

import homework.step.Menu;

public class MainService {
    public String menuContent() {
        return new Menu().show();
    }
}
