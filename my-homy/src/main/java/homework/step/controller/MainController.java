package homework.step.controller;

import homework.step.service.MainService;

public class MainController {
    private final MainService mainService;

    public MainController() {
        this.mainService = new MainService();
    }

    public String help() {
        return mainService.menuContent();
    }
}
